class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @root_taxon = Spree::Taxon.root
  end
end
