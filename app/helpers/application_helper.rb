module ApplicationHelper
  def bigbag_store_title(name: "")
    name.blank? ? Const::BIGBAG_STORE : "#{name} - #{Const::BIGBAG_STORE}"
  end
end
