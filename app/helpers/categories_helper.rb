module CategoriesHelper
  def tree_taxon(descendants, level, taxon)
    if taxon.leaf?
      render "leaf_taxon", taxon: taxon
    else
      render "parent_taxon", descendants: descendants, current_level: level + 1, taxon: taxon
    end
  end
end
