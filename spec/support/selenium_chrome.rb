Capybara.register_driver :selenium_chrome_headless.to_sym do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome,
  desired_capabilities: Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: {
      args: ["--headless", "--no-sandbox", "--incognito", "--disable-dev-shm-usage"],
      w3c: false,
    }
  ))
end
