require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#bigbag_store_title' do
    subject { bigbag_store_title(name: name) }

    context 'when argument is present' do
      let(:name) { 'sample' }

      it { is_expected.to eq 'sample - BIGBAG Store' }
    end

    context 'when argument is empty string' do
      let(:name) { '' }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context 'when arguent is nil' do
      let(:name) { nil }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context 'when argument is not exit' do
      subject { bigbag_store_title }

      it { is_expected.to eq 'BIGBAG Store' }
    end
  end
end
