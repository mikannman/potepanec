require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe 'potepan/products resources' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    describe 'GET #show' do
      before do
        get potepan_product_path(product.id)
      end

      it 'returns http 200' do
        expect(response).to have_http_status(200)
      end

      describe 'check instance variables' do
        it 'is @product' do
          expect(response.body).to include product.name
          expect(response.body).to include "#{product.display_price}"
          expect(response.body).to include product.description
        end
      end
    end
  end
end
