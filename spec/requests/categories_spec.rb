require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe 'potepan/categories resources' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    describe 'GET #show' do
      before do
        get potepan_category_path(taxon.id)
      end

      it 'returns http 200' do
        expect(response).to have_http_status(200)
      end

      describe 'check instance variables' do
        it 'is @taxonomies' do
          expect(response.body).to include taxon.name
          expect(response.body).to include 'Brand'
        end

        it 'is @taxon' do
          expect(response.body).to include taxon.name
        end

        it 'is @products' do
          expect(response.body).to include product.name
          expect(response.body).to include "#{product.display_price}"
        end
      end
    end
  end
end
