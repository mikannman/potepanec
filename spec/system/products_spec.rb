require 'rails_helper'

RSpec.describe "Products", type: :system do
  describe 'user visits show page' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_without_category) { create(:product) }

    it 'is displayed product information' do
      visit potepan_product_path(product.id)

      within('#top_lightSection') do
        expect(page).to have_content(product.name, count: 2)
      end

      within('#product_show_media_body') do
        expect(page).to have_content(product.name)
        expect(page).to have_content(product.display_price)
        expect(page).to have_content(product.description)
      end
    end

    describe 'user clicks some link' do
      it 'is home link' do
        visit potepan_product_path(product.id)
        click_link 'show_page_home_link'

        expect(current_path).to eq(potepan_root_path)
      end

      context 'when product has category' do
        it 'is link returning to category page' do
          visit potepan_product_path(product.id)
          click_link '一覧ページへ戻る'

          expect(current_path).to eq(potepan_category_path(product.taxons.first.id))
        end
      end

      context 'when product does not have category' do
        it 'is link returning to category page' do
          visit potepan_product_path(product_without_category.id)
          click_link '一覧ページへ戻る'

          expect(current_path).to eq(potepan_category_path(Spree::Taxon.root.id))
        end
      end
    end
  end
end
