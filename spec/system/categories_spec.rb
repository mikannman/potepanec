require 'rails_helper'

RSpec.describe "Categories", type: :system do
  describe 'user visits show page' do
    let!(:parent_taxon) { create(:taxon) }
    let!(:child_taxon_1) { create(:taxon, name: 'taxon_1', parent_id: parent_taxon.id) }
    let!(:child_taxon_2) { create(:taxon, name: 'taxon_2', parent_id: parent_taxon.id) }
    let!(:grandchild_taxon) { create(:taxon, name: 'taxon_3', parent_id: child_taxon_1.id) }
    let!(:product_1) { create(:product, name: 'product_1', price: 1, taxons: [child_taxon_1]) }
    let!(:product_2) { create(:product, name: 'product_2', price: 2, taxons: [child_taxon_2]) }

    context 'when it is parent category page' do
      it 'is displayed parent category name and all products' do
        visit potepan_category_path(parent_taxon.id)

        within('#top_lightSection') do
          expect(page).to have_content(parent_taxon.name, count: 2)
        end

        within('#category_show_product_caption') do
          expect(page).to have_content(product_1.name)
          expect(page).to have_content(product_1.display_price)
          expect(page).to have_content(product_2.name)
          expect(page).to have_content(product_2.display_price)
        end
      end
    end

    context 'when it is child category page' do
      it 'is displayed child category name and only having products' do
        visit potepan_category_path(child_taxon_1.id)

        within('#top_lightSection') do
          expect(page).to have_content(child_taxon_1.name, count: 2)
        end

        within('#category_show_product_caption') do
          expect(page).to have_content(product_1.name)
          expect(page).to have_content(product_1.display_price)
          expect(page).not_to have_content(product_2.name)
          expect(page).not_to have_content(product_2.display_price)
        end
      end
    end

    describe 'user clicks some link' do
      it 'is category link' do
        visit potepan_category_path(parent_taxon.id)
        click_link "descendants_taxon_#{child_taxon_1.id}_for_test"

        expect(current_path).to eq(potepan_category_path(child_taxon_1.id))
      end

      it 'is product name link' do
        visit potepan_category_path(parent_taxon.id)
        click_link "#{product_1.name}"

        expect(current_path).to eq(potepan_product_path(product_1.id))
      end

      it 'is product price link' do
        visit potepan_category_path(parent_taxon.id)
        click_link "#{product_1.display_price}"

        expect(current_path).to eq(potepan_product_path(product_1.id))
      end
    end

    describe 'js operation check' do
      context 'when user does not click parent category in the sidebar' do
        it 'is not displayed child_taxon_name', js: true do
          visit potepan_category_path(parent_taxon.id)

          expect(page).not_to have_content(child_taxon_1.name)
        end
      end

      context 'when user clicks parent category in the sidebar' do
        it 'is displayed child_taxon_name', js: true do
          visit potepan_category_path(parent_taxon.id)
          click_on "root_taxon_#{parent_taxon.id}_for_test"

          expect(page).to have_content(child_taxon_1.name)
          expect(page).not_to have_content(grandchild_taxon.name)
        end
      end

      context 'when user clicks child category has grandchild in the sidebar' do
        it 'is displayed grand_child_taxon_name', js: true do
          visit potepan_category_path(parent_taxon.id)
          click_on "root_taxon_#{parent_taxon.id}_for_test"

          expect(page).to have_content(child_taxon_1.name)

          click_on "parent_taxon_#{child_taxon_1.id}_for_test"

          expect(page).to have_content(grandchild_taxon.name)
        end
      end
    end
  end
end
